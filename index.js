function getEle(tag) {
  return document.querySelector(tag);
}

var dsnv = [];

function themNV() {
  var newNV = layThongTinTuForm();

  var isValid = 
  validator.kiemTraRong(
    newNV.taiKhoan,
    "#tbTKNV",
    "Tài khoản không được để rỗng"
  )&& validator.kiemTraChuoiSo(newNV.taiKhoan,"#tbTKNV")
  && validator.kiemTraDoDai(newNV.taiKhoan,"#tbTKNV",4,6);

  isValid =
    isValid &
    validator.kiemTraRong(newNV.hoTen, "#tbTen", "Họ tên không được rỗng") 
    && validator.kiemTraChuoiChu(newNV.hoTen,"#tbTen");

  isValid =
    isValid &
    validator.kiemTraRong(newNV.email, "#tbEmail", "Email không được rỗng")
    && validator.kiemTraEmail(newNV.email,"#tbEmail");

  isValid =
    isValid &
    validator.kiemTraRong(
      newNV.matKhau,
      "#tbMatKhau",
      "Mật khẩu không được rỗng"
    )&& validator.kiemTraDoDai(newNV.matKhau,"#tbMatKhau",6,10)&&
    validator.kiemTraMatKhau(newNV.matKhau,"#tbMatKhau");

  isValid =
    isValid &
    validator.kiemTraRong(newNV.ngayLam, "#tbNgay", "Ngày không được rỗng")&&
    validator.kiemTraNgay(newNV.ngayLam,"#tbNgay");

  isValid =
    isValid &
    validator.kiemTraRong(
      newNV.luongCoBan,
      "#tbLuongCB",
      "Lương không được rỗng"
    )&& validator.kiemTraChuoiSo(newNV.luongCoBan,"#tbLuongCB")&&
    validator.kiemTraGiaTri(newNV.luongCoBan,"#tbLuongCB",1000000,20000000);

  isValid =
    isValid &
    validator.kiemTraRong(newNV.chucVu, "#tbChucVu", "Chức vụ không được rỗng");

  isValid =
    isValid &
    validator.kiemTraRong(newNV.gioLam, "#tbGiolam", "Giờ làm không được rỗng")
    && validator.kiemTraChuoiSo(newNV.gioLam,"#tbGiolam")&& 
    validator.kiemTraGiaTri(newNV.gioLam,"#tbGiolam",80,200);

  if (isValid == false) {
    getEle("#btnThemNV").removeAttribute('data-dismiss');
    var listSpan = document.querySelectorAll('.sp-thongbao');
    for (var i = 0; i < listSpan.length ; i++){
      listSpan[i].style.display = 'block';
    }
    return;
  } else {
    getEle("#btnThemNV").setAttribute('data-dismiss','modal');
  }
  dsnv.push(newNV);

  renderDanhSachNhanVien(dsnv);
}
// console.log("dsnv: ", dsnv);

function xoaNV(taiKhoan) {
  var i = timViTriNhanVien(taiKhoan, dsnv);
  if (i !== -1) {
    dsnv.splice(i, 1);

    renderDanhSachNhanVien(dsnv);
  }
}

function suaNV(taiKhoan) {
  var i = timViTriNhanVien(taiKhoan, dsnv);
  if (i !== -1) {
    var nv = dsnv[i];

    showThongTinLenForm(nv);
  }
}

function capNhatNV() {
  var nvEdit = layThongTinTuForm();

  var i = timViTriNhanVien(nvEdit.taiKhoan, dsnv);
  if (i !== -1) {
    dsnv[i] = nvEdit;

  var isValid = 
  validator.kiemTraRong(
    nvEdit.taiKhoan,
    "#tbTKNV",
    "Tài khoản không được để rỗng"
  )&& validator.kiemTraChuoiSo(nvEdit.taiKhoan,"#tbTKNV")
  && validator.kiemTraDoDai(nvEdit.taiKhoan,"#tbTKNV",4,6);

  isValid =
    isValid &
    validator.kiemTraRong(nvEdit.hoTen, "#tbTen", "Họ tên không được rỗng") 
    && validator.kiemTraChuoiChu(nvEdit.hoTen,"#tbTen");

  isValid =
    isValid &
    validator.kiemTraRong(nvEdit.email, "#tbEmail", "Email không được rỗng")
    && validator.kiemTraEmail(nvEdit.email,"#tbEmail");

  isValid =
    isValid &
    validator.kiemTraRong(
      nvEdit.matKhau,
      "#tbMatKhau",
      "Mật khẩu không được rỗng"
    )&& validator.kiemTraDoDai(nvEdit.matKhau,"#tbMatKhau",6,10)&&
    validator.kiemTraMatKhau(nvEdit.matKhau,"#tbMatKhau");

  isValid =
    isValid &
    validator.kiemTraRong(nvEdit.ngayLam, "#tbNgay", "Ngày không được rỗng")&&
    validator.kiemTraNgay(nvEdit.ngayLam,"#tbNgay");

  isValid =
    isValid &
    validator.kiemTraRong(
      nvEdit.luongCoBan,
      "#tbLuongCB",
      "Lương không được rỗng"
    )&& validator.kiemTraChuoiSo(nvEdit.luongCoBan,"#tbLuongCB")&&
    validator.kiemTraGiaTri(nvEdit.luongCoBan,"#tbLuongCB",1000000,20000000);

  isValid =
    isValid &
    validator.kiemTraRong(nvEdit.chucVu, "#tbChucVu", "Chức vụ không được rỗng");

  isValid =
    isValid &
    validator.kiemTraRong(nvEdit.gioLam, "#tbGiolam", "Giờ làm không được rỗng")
    && validator.kiemTraChuoiSo(nvEdit.gioLam,"#tbGiolam")&& 
    validator.kiemTraGiaTri(nvEdit.gioLam,"#tbGiolam",80,200);

  if (isValid == false) {
    getEle("#btnCapNhat").removeAttribute('data-dismiss');
    var listSpan = document.querySelectorAll('.sp-thongbao');
    for (var i = 0; i < listSpan.length ; i++){
      listSpan[i].style.display = 'block';
    }
    return;
  } else {
    getEle("#btnCapNhat").setAttribute('data-dismiss','modal');
  }

    renderDanhSachNhanVien(dsnv);
  }
}

var dsnvTheoLoai = [];
function timNhanVien(){
  var loai = getEle("#searchName").value.trim();
  for(var i = 0; i < dsnv.length;i++){
    if(dsnv[i].xepLoai() == loai){
      dsnvTheoLoai.push(dsnv[i]);
    } 
  }

  renderDanhSachNhanVien(dsnvTheoLoai);
}