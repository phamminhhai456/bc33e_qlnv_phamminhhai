function NhanVien(
  taiKhoan,
  hoTen,
  email,
  matKhau,
  ngayLam,
  luongCoBan,
  chucVu,
  gioLam
) {
  this.taiKhoan = taiKhoan;
  this.hoTen = hoTen;
  this.email = email;
  this.matKhau = matKhau;
  this.ngayLam = ngayLam;
  this.luongCoBan = luongCoBan;
  this.chucVu = chucVu;
  this.gioLam = gioLam;
  this.tongLuong = function () {
    if (chucVu == "Sếp") {
      return this.luongCoBan * 3;
    }
    if (chucVu == "Trưởng phòng") {
      return this.luongCoBan * 2;
    }
    if (chucVu == "Nhân viên") {
      return this.luongCoBan * 1;
    }
  };
  this.xepLoai = function () {
    var loai = "";
    if (this.gioLam >= 192) {
      loai = "xuất sắc";
    } else if (this.gioLam >= 176 && this.gioLam < 192) {
      loai = "giỏi";
    } else if (this.gioLam >= 160 && this.gioLam < 176) {
      loai = "khá";
    } else {
      loai = "trung bình";
    }
    return loai;
  };
}
