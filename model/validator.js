function getEle(tag) {
  return document.querySelector(tag);
}

var validator = {
  kiemTraRong: function (valueInput, idError, message) {
    if (valueInput.trim() == "") {
      getEle(idError).innerText = message;
      return false;
    } else {
      getEle(idError).innerText = "";
      return true;
    }
  },
  kiemTraChuoiSo: function (valueInput, idError) {
    regex = /^[0-9]+$/;

    if (regex.test(valueInput)) {
      getEle(idError).innerText = "";
      return true;
    } else {
      getEle(idError).innerText = "Trường này chỉ được nhập số";
      return false;
    }
  },
  kiemTraChuoiChu: function (valueInput, idError) {
    regex =
      /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
    if (regex.test(valueInput)) {
      getEle(idError).innerText = "";
      return true;
    } else {
      getEle(idError).innerText = "Trường này chỉ được nhập chữ";
      return false;
    }
  },
  kiemTraDoDai: function (valueInput, idError, min, max) {
    var inputLength = valueInput.length;
    if (inputLength < min || inputLength > max) {
      getEle(idError).innerText = `Độ dài phải từ ${min} - ${max} kí tự`;
      return false;
    } else {
      getEle(idError).innerText = "";
      return true;
    }
  },
  kiemTraEmail: function (valueInput, idError) {
    regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (regex.test(valueInput)) {
      getEle(idError).innerText = "";
      return true;
    } else {
      getEle(idError).innerText = "Email phải đúng định dạng";
      return false;
    }
  },
  kiemTraMatKhau: function (valueInput, idError) {
    regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])/;

    if (regex.test(valueInput)) {
      getEle(idError).innerText = "";
      return true;
    } else {
      getEle(idError).innerText =
        "Mật khẩu phải có số, ký tự in hoa và ký tự đặc biệt";
      return false;
    }
  },
  kiemTraNgay: function (valueInput, idError) {
    regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;

    if (regex.test(valueInput)) {
      getEle(idError).innerText = "";
      return true;
    } else {
      getEle(idError).innerText = "Ngày phải theo định dạng mm/dd/yyyy";
      return false;
    }
  },
  kiemTraGiaTri: function (valueInput, idError, min, max) {
    if (valueInput * 1 < min || valueInput * 1 > max) {
      getEle(idError).innerText = `Giá trị phải từ ${min} - ${max}`;
      return false;
    } else {
      getEle(idError).innerText = "";
      return true;
    }
  },
};
