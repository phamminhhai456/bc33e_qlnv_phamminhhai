function getEle(tag) {
  return document.querySelector(tag);
}

function layThongTinTuForm() {
  var taiKhoan = getEle("#tknv").value.trim();
  var hoTen = getEle("#name").value.trim();
  var email = getEle("#email").value.trim();
  var matKhau = getEle("#password").value.trim();
  var ngayLam = getEle("#datepicker").value;
  var luongCoBan = getEle("#luongCB").value;
  var chucVu = getEle("#chucvu").value;
  var gioLam = getEle("#gioLam").value;

  var nv = new NhanVien(
    taiKhoan,
    hoTen,
    email,
    matKhau,
    ngayLam,
    luongCoBan,
    chucVu,
    gioLam
  );
  console.log("nv: ", nv);
  return nv;
}

function renderDanhSachNhanVien(list) {
  var contentHtml = "";

  list.forEach(function (item) {
    var content = `<tr>
        <td>${item.taiKhoan}</td>
        <td>${item.hoTen}</td>
        <td>${item.email}</td>
        <td>${item.ngayLam}</td>
        <td>${item.chucVu}</td>
        <td>${item.tongLuong()}</td>
        <td>${item.xepLoai()}</td>
        <td>
        <button class="btn btn-danger" onclick="xoaNV(${
          item.taiKhoan
        })">Xóa</button>
        <br/>
        <button 
        class="btn btn-warning"
        data-toggle="modal"
        data-target="#myModal"
        onclick="suaNV(${item.taiKhoan})"
        >Sửa</button>
        </td>
    </tr>`;
    contentHtml += content;
  });

  getEle("#tableDanhSach").innerHTML = contentHtml;
}

function timViTriNhanVien(taiKhoan, arr) {
  return arr.findIndex(function (nv) {
    return nv.taiKhoan == taiKhoan;
  });
}

function showThongTinLenForm(nv) {
  getEle("#tknv").value = nv.taiKhoan;
  getEle("#name").value = nv.hoTen;
  getEle("#email").value = nv.email;
  getEle("#password").value = nv.matKhau;
  getEle("#datepicker").value = nv.ngayLam;
  getEle("#luongCB").value = nv.luongCoBan;
  getEle("#chucvu").value = nv.chucVu;
  getEle("#gioLam").value = nv.gioLam;
}
